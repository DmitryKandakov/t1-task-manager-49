package ru.t1.dkandakov.tm.jms.operation;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}
