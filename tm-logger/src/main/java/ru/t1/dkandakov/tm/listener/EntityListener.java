package ru.t1.dkandakov.tm.listener;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.service.LoggerService;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public final class EntityListener implements MessageListener {

    @NotNull
    private final LoggerService loggerService;

    public EntityListener(@NotNull final LoggerService loggerService) {
        this.loggerService = loggerService;
    }

    @Override

    public void onMessage(Message message) {
        final boolean checkType = message instanceof TextMessage;
        if (!checkType) return;
        @NotNull final TextMessage textMessage = (TextMessage) message;
        @NotNull final String yaml;
        try {
            yaml = textMessage.getText();
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
        loggerService.log(yaml);
    }

}
